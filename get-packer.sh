PACKER_FILE='packer_0.8.6_linux_amd64.zip'


function failed_checksum {
	echo "Failed to verify checksum!"
	exit 2
}






if [[ $(uname -m) != "x86_64" ]]; then
	echo "Only x64 systems supported. You're like what in the stoneage?"
	exit 1
fi

FOLDER='packer'
if [[ ( -e ${FOLDER} ) && !( -d ${FOLDER} ) ]]; then
	echo "'${FOLDER}' already exists and is no directory"
	exit 4
elif [[ !( -e ${FOLDER} ) ]]; then
	mkdir ${FOLDER}
fi

if [[ -d ${PACKER_FILE} ]]; then
	rm -rf ./packer/${PACKER_FILE%zip}
fi


case "$OSTYPE" in
	"linux-gnu")
		if [[ -n $(which curl)  ]]; then
			cd packer
			curl -q --ssl-reqd --url "https://dl.bintray.com/mitchellh/packer/packer_0.8.6_SHA256SUMS" -o "${PACKER_FILE}_SHA256SUMS"
			cat "${PACKER_FILE}_SHA256SUMS" | grep $PACKER_FILE > "${PACKER_FILE}_SHA256SUMS.file"
			mv "${PACKER_FILE}_SHA256SUMS.file" "${PACKER_FILE}_SHA256SUMS"
			if [[ -e ${PACKER_FILE} ]]; then
				sha256sum --quiet --strict -c "${PACKER_FILE}_SHA256SUMS"
			fi
			if [[ !( -e ${PACKER_FILE} ) ||  $? -ne 0 ]]; then
				curl -q -J -L --ssl-reqd --url "https://dl.bintray.com/mitchellh/packer/packer_0.8.6_linux_amd64.zip" -o "${PACKER_FILE}"
				sha256sum --quiet --strict -c "${PACKER_FILE}_SHA256SUMS"
			fi
			if [[ $? -ne 0 ]]; then
				failed_checksum
			fi
		elif [[ -n $(which wget) ]]; then
			echo "wget soon"
		else
			echo "Neither 'wget' nor 'curl' are available. Can't download 'packer'!"
			exit 3
		fi


		;;
	"freebsd*")
		echo "Please implement me :)"
		;;
	*)

		;;
esac

unzip -d '../bin' ${PACKER_FILE} 
